package com.example.frutasapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;



public class MainActivity2_Nivel2 extends AppCompatActivity {


    private TextView tvnombre1, tvScore1, tvBestScore;
    private ImageView tvVidas1;
    private ImageView ivAuno1, ivAdos1, ivVidas1;
    private EditText etRespuestas1;
    private MediaPlayer mp, mpGreat, mpBad;
    int score, numeroAleatorio1, numeroAleatorio2, resultado, vidas = 3;
    String nombreJugador, stringScore, stringVidas;
    String numero[] = {"cero", "uno", "dos", "tres", "cuatro", "cinco", "seis", "siete", "ocho", "nueve"};


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_activity2__nivel1);

        Toast.makeText(this, "Nivel 2--Sumas con mayor dificultad", Toast.LENGTH_SHORT).show();

        tvnombre1 = (TextView) findViewById(R.id.tvnombre);
        tvScore1 = (TextView) findViewById(R.id.tvScore);
        ivVidas1 = (ImageView) findViewById(R.id.ivVidas);
        ivAuno1 = (ImageView) findViewById(R.id.ivAuno);
        ivAdos1 = (ImageView) findViewById(R.id.ivAdos);
        etRespuestas1 = (EditText) findViewById(R.id.etResultado);
        tvBestScore = (TextView) findViewById(R.id.tvBestScore);

        nombreJugador = getIntent().getStringExtra("jugador");
        tvnombre1.setText("Jugador: " + nombreJugador);

        stringScore=getIntent().getStringExtra("score");
        score=Integer.parseInt(stringScore);
        tvScore1.setText("score: "+score);

        stringVidas=getIntent().getStringExtra("vidas");


        if (vidas==3){
            ivVidas1.setImageResource(R.drawable.tresvidas);
        }  if (vidas==2){
            ivVidas1.setImageResource(R.drawable.dosvidas);
        } if (vidas==1){
            ivVidas1.setImageResource(R.drawable.unavida);
        }


        //agregar el icono en el action bar de esta activity
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        score=Integer.parseInt(stringScore);
        getSupportActionBar().setIcon(R.mipmap.ic_launcher);

        //agregar la musica a esta activity
        mp = MediaPlayer.create(this, R.raw.goats);
        mp.start();
        mp.setLooping(true);

        mpGreat = MediaPlayer.create(this, R.raw.wonderful);//sonido cuando acierta
        mpBad = MediaPlayer.create(this, R.raw.bad);//sonido cuando se equivoca

        NumeroAleatorio();

    } //fin del metodo oncreate

    //metodo comparar
    public void Comparar(View view) {
        String respuesta = etRespuestas1.getText().toString();

        if (!respuesta.equals("")) {

            int respuestaJugador = Integer.parseInt(respuesta);
            if (resultado == respuestaJugador) {
                mpGreat.start();
                score++;
                tvScore1.setText("Score: " + score);
                etRespuestas1.setText("");
                BaseDeDatos();

            } else {
                mpBad.start();
                vidas--;
                BaseDeDatos();

                switch (vidas) {
                    case 3:
                        ivVidas1.setImageResource(R.drawable.tresvidas);
                        break;
                    case 2:
                        Toast.makeText(this, "Te quedan dos vidas", Toast.LENGTH_SHORT).show();
                        ivVidas1.setImageResource(R.drawable.dosvidas);
                        break;
                    case 1:
                        Toast.makeText(this, "Te queda 1 vida", Toast.LENGTH_SHORT).show();
                        ivVidas1.setImageResource(R.drawable.unavida);
                        break;
                    case 0:
                        Toast.makeText(this, "Has perdido todas tus manzanas", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(this, MainActivity.class);
                        startActivity(intent);
                        finish();
                        mp.stop();
                        mp.release();//liberar recursos
                        break;


                }
                etRespuestas1.setText("");

            }
            NumeroAleatorio();

        } else {
            Toast.makeText(this, "Escribe tu respuesta", Toast.LENGTH_SHORT).show();
        }

    }

    //metodo numeros aleatorios. No lleva view porque no corresponde a ningun boton
    public void NumeroAleatorio() {
        if (score <=19) {

            numeroAleatorio1 = (int) (Math.random() * 10);
            numeroAleatorio2 = (int) (Math.random() * 10);

            resultado = numeroAleatorio1 + numeroAleatorio2;



                for (int i = 0; i < numero.length; i++) {
                    int id = getResources().getIdentifier(numero[i], "drawable", getPackageName());
                    if (numeroAleatorio1 == i) {
                        ivAuno1.setImageResource(id);

                    }
                    if (numeroAleatorio2 == i) {
                        ivAdos1.setImageResource(id);


                    }

                }



        } else {
            Intent intent = new Intent(this,MainActivity2_Nivel3.class);
            stringScore = String.valueOf(score);
            stringVidas = String.valueOf(vidas);
            intent.putExtra("jugador", nombreJugador);
            intent.putExtra("score", stringScore);
            intent.putExtra("vidas", vidas);
            startActivity(intent);
            finish();
            mp.stop();

        }

    }

    public void BaseDeDatos() {
        AdminSQLiteOpenHelper admin = new AdminSQLiteOpenHelper(this, "bd", null, 2);

        SQLiteDatabase bd = admin.getReadableDatabase();

        Cursor consulta = bd.rawQuery("select  * from puntaje where score=  (select max(score) from puntaje)", null);
        if (consulta.moveToFirst()) {
            String temp_nombre = consulta.getString(0);
            String tem_score = consulta.getString(1);
            int bestScore = Integer.parseInt(tem_score);

            if (score > bestScore) {
                ContentValues modificacion = new ContentValues();
                modificacion.put("nombre", nombreJugador);
                modificacion.put("score", score);
                bd.update("puntaje", modificacion, "score=" + bestScore, null);
            }

            bd.close();
        } else {
            ContentValues insertar = new ContentValues();
            insertar.put("nombre", nombreJugador);
            insertar.put("score", score);
            bd.insert("puntaje", null, insertar);

        }
        bd.close();
    }

    //codigo para no permitir regresar con el boton back al nivel anterior(1)
    @Override
    public void onBackPressed() {

    }

}