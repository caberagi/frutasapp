package com.example.frutasapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private EditText etNombre1;
    private ImageView imgPersonaje1;
    private TextView tvBestScore1;
    private MediaPlayer mp;

    int numeroAleatorio = (int) (Math.random() * 10);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        etNombre1 = (EditText) findViewById(R.id.etNombre);
        imgPersonaje1 = (ImageView) findViewById(R.id.imageViewPersonaje);
        tvBestScore1 = (TextView) findViewById(R.id.tvBestScore);

        //linea de codigo que me permite colocar la  imagen o el icono dentro del action bar

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.mipmap.ic_launcher);

        int id;

        if (numeroAleatorio == 0 || numeroAleatorio == 10) {
            id = getResources().getIdentifier("mango", "drawable", getPackageName()); //ruta para cambiar de imagenes al principio
            imgPersonaje1.setImageResource(id);

        } else if (numeroAleatorio == 1 || numeroAleatorio == 9) {
            id = getResources().getIdentifier("fresa", "drawable", getPackageName()); //ruta para cambiar de imagenes al principio
            imgPersonaje1.setImageResource(id);
        } else if (numeroAleatorio == 2 || numeroAleatorio == 8) {
            id = getResources().getIdentifier("manzana", "drawable", getPackageName()); //ruta para cambiar de imagenes al principio
            imgPersonaje1.setImageResource(id);
        } else if (numeroAleatorio == 3 || numeroAleatorio == 7) {
            id = getResources().getIdentifier("sandia", "drawable", getPackageName()); //ruta para cambiar de imagenes al principio
            imgPersonaje1.setImageResource(id);

        } else if (numeroAleatorio ==4 || numeroAleatorio == 5 || numeroAleatorio == 6) {
            id = getResources().getIdentifier("uva", "drawable", getPackageName()); //ruta para cambiar de imagenes al principio
            imgPersonaje1.setImageResource(id);

        }
        //CONEXION DE LA BASE DE DATOS BD
        AdminSQLiteOpenHelper admin =new AdminSQLiteOpenHelper(this,"bd",null,2);
        SQLiteDatabase bd=admin.getWritableDatabase();
        //codigo para  traer el mayor puntaje de un jugador
        Cursor consulta  = bd.rawQuery("select  *  from puntaje where score =(select max(score) from puntaje)",null );
        if (consulta.moveToFirst()){

            String tempNombre= consulta.getString(0);
            String tempScore= consulta.getString(1);
            tvBestScore1.setText("Record: "+ tempScore +" de: "+ tempNombre);
            bd.close(); //cerramos la base de datos
        }  else {
            bd.close();
        }

        //codigo para insertar la pista de audio
        mp=MediaPlayer.create(this,R.raw.alphabet_song);
        mp.start();
        mp.setLooping(true);
    }
    //metodo boton jugar
    public void Jugar(View view){

        String nombre=etNombre1.getText().toString();

        if (!nombre.equals(" ")){
            mp.stop();
            mp.release(); //importante linea para no cargar la aplicacion

            Intent intent=new Intent(this,MainActivity2_Nivel1.class);

            intent.putExtra("jugador",nombre);
            startActivity(intent);
            finish();

        } else {

            Toast.makeText(this,"Primero debes escribir tu nombre",Toast.LENGTH_SHORT).show();
            //CODIGO PARA ABRIR EL TECLADO CUANDO EL USUARIO NO ESCRIBA NADA
            etNombre1.requestFocus();
            InputMethodManager imm =(InputMethodManager)getSystemService(this.INPUT_METHOD_SERVICE);
            imm.showSoftInput(etNombre1,InputMethodManager.SHOW_IMPLICIT);
        }
    }
    //SOBREESCRIBIR EL METODO

    @Override
    public void onBackPressed(){

    }
}